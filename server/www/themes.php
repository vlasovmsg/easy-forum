<?php
// Подключем шапку.
include "header.php";
?>


    <table class="table table-striped table-hover caption-top">
        <caption>Сообщения</caption>
        <thead class="table-dark">
        <tr>
            <th class="message_author" scope="col">Автор</th>
            <th  class="message_text" scope="col">Сообщения</th>
        </tr>
        </thead>
        <tbody>


        <?php

        // ---------------------
        // Получим текущий адрес для формирование ссылки на возрат
        $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        // Для дальнейшего удобства заведем тему в отдельную переменную.
        $themes_id = $_GET['id'];

        // ---------------------
        // Подготовлаиваем переменные для постраничной навигации.
        // Проверим на какой мы странице и присвоим номер переменной page.
        isset($_GET['page']) ? $page = $_GET['page']: $page = 1;
        // Количество записей на странице для лимита в запрос.
        $count_items_page = 10;
        // Получим общее число сообщений в конкретной теме.
        $count_items_all = db::getValue("SELECT COUNT(*) AS count FROM messages WHERE messages.themes = :themes", array('themes' => $themes_id));
        // Вычисляем номер первой записи на необходимой странице для установки лимита в запрос.
        $start_item = ($page * $count_items_page) - $count_items_page;
        // Посчитаем сколько нам нужно страниц и округлим до целого в большую сторону.
        $count_page = ceil($count_items_all / $count_items_page);

        // ---------------------
        // Запрос на получение сообщений в конкретной теме.
        $messages_items = db::getAll("
        SELECT users.name AS user_name,                        -- Имя пользователя.
            users.date AS user_date,                           -- Дата регистрации пользователя.
            table2.user_messages_count AS user_messages_count, -- Количество сообщений суммарно оставленных пользователем на сайте.
            messages.text AS messages_text,                    -- Текст сообщения.
            messages.date AS messages_date                     -- Дата сообщения.
        FROM messages               
            LEFT JOIN                                      -- Сводим таблицу с таблицой table2 полученной в запросе c авторами и количеством сообщений каждого.
                (SELECT COUNT(messages2.id) AS user_messages_count, -- Количество сообщенний оставленных автором.
                    messages2.author AS author2                     -- Автор сообщений для дальнейшей связи с внешним запросом.
                FROM messages AS messages2                 -- Обязательный псевдоним.  
                GROUP BY messages2.author)                 -- Группируем по автору для вычисления суммы сообщений.
            AS table2                                      -- Присваевам запросу алиас для дальнейшей работы с резульатами в текущем запросе.
            ON messages.author = table2.author2            -- Для получения суммарного количества сообщений оставленных пользователем на форуме.
            LEFT JOIN users ON users.id = messages.author  -- Для получения имени пользователя сообщений.
        WHERE messages.themes = :themes      -- Условие отбора сообщений по конкретной теме.
        ORDER BY messages_date ASC           -- Сортируем по дате сообщения в порядке возрастания.
        LIMIT $start_item, $count_items_page -- Ограничиваем число записей.
        ", array('themes' => $themes_id));  // Параметр отбора.

        // ---------------------
        // Перебираем полученный после запроса массив данных.
        foreach ($messages_items as $item) {
            // Загоняем полученный из массива данные в переменные для более читабельного вида при выводе с помощью echo.
            // Имя пользователя.
            $user_name = $item['user_name'];
            // Читабельный формат даты регистрации пользователя.
            $user_date = date("d.m.y", strtotime($item['user_date']));
            // Количество оставленных пользователем на форуме сообщений.
            $user_messages_count = $item['user_messages_count'];
            // Формируем данные о пользователе для вывода одним блоком.
            $user_data = "$user_name<br>На сайте с $user_date<br>Всего сообщений ($user_messages_count)";
            // Текст сообщения.
            $message_text = $item['messages_text'];
            // Читабельный формат даты отправки сообщения.
            $message_date = date("d.m.y H:i:s", strtotime($item['messages_date']));

            // Выводим подготовленную информацию.
            echo<<<html
        <tr>
            <th class="message_author cell" scope="row">$user_data</th>
            <td class="message_text cell"><mark>$message_date</mark><br>$message_text</td>
        </tr>
        html;
        }
        ?>


        </tbody>
    </table>

    <nav>
        <ul class="pagination justify-content-center">


            <?php
            // ---------------------
            // Формируем навигацию.
            // Проверяем, если страница не первая, то добавляем слева ссылку на первую.
            if ($page > 1) { echo "<li class=\"page-item\"><a  class=\"page-link\" href=\"themes.php?id=$themes_id&page=1\"> Первая </a></li>"; }

            // Если страниц меньше 3 то нет смысла придумывать переключение страниц.
            if ($count_page > 2) {
                // Хитрости с переключением страниц, дложно быть всегда 3 старинцы для переключения, а текущая была по середине.
                // Допусти у нас страница X тогда логично сделать слева страницу X - 1 а с права X + 1.
                $first_page = $page - 1;
                $last_page = $page + 1;

                // Проверим, если страница X - 1 получилась <= 0 тогда это первая страница и остальные 2 будут справа.
                if ($first_page <= 0) {
                    $first_page = 1;
                    $last_page = $last_page + 1;
                }
                // А если страница X - 1 получилась >= последней странице тогда это последняя страница и остальные 2 будут слева.
                if ($last_page >= $count_page) {
                    $first_page = $count_page - 2;
                    $last_page = $count_page;
                }

                // С полученными страницами мы формируем ссылки через цикл.
                for ($i = $first_page; $i <= $last_page; $i++) {// Проверим, если i это текущая страница то выводим ее как текст а не как ссылку.
                    if ($i == $page) {
                        echo "<li class=\"page-item active\"><a  class=\"page-link\"> $i из $count_page </a></li>";
                    } else {
                        echo "<li class=\"page-item\"><a  class=\"page-link\" href=\"themes.php?id=$themes_id&page=$i\"> $i </a></li>";
                    }
                }
            }

            // Если страница не последняя то добавляем ссылку справа на последнюю.
            if ($page < $count_page) { echo "<li class=\"page-item\"><a  class=\"page-link\" href=\"themes.php?id=$themes_id&page=$count_page\"> Последняя </a></li>"; }
            ?>


        </ul>
    </nav>


    <div class="justify-content-center">
        <h5>Форма добавления сообщения</h5>
        <form action="action_message.php" method="post">
            <div class="mb-3">
                <label for="message_text" class="form-label">Сообщение</label>
                <input id="message_text" class="form-control" name="message_text" type="text" placeholder="Сообщение">
                <input name="message_themes" type="hidden" value="<?php echo $_GET['id'] ?>"/>
                <input name="url" type="hidden" value="<?php echo $url ?>"/>
            </div>


            <?php
            // Если у нас сохранилась ссесия то выводить форму для входа пользователя не обязательно.
            if (!isset($_COOKIE["user_name"])) {
                echo<<<html
                <div class="mb-3">
                    <label for="user_name" class="form-label">Имя пользователя</label>
                    <input id="user_name" class="form-control" type="text" name="user_name">
                </div>
                <div class="mb-3">
                    <label for="user_email" class="form-label">E-mail</label>
                    <input id="user_email" class="form-control" type="email" name="user_email">
                </div>
                html;
            }
            ?>


            <input class="btn btn-primary" type="submit" value="Отправить">
        </form>
    </div>

<?php
//Подключем подвал.
include "footer.php";
?>
