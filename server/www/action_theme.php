<?php
require_once 'functions.php';
// Подключем шапку.
include "header.php";

// Проверяем, если пользователь авторизован то добавляем только тему, если нет,
// то тему и пользователя, заодно авторизовываем его.

//Условие первое, если пользователь авторизован то просто добавляем тему.
if (isset($_COOKIE["user_name"])) {
    if (!empty($_POST['themes_header'])) {
        add_themes($_POST['themes_header'], $_COOKIE["user_id"]);
        message_good();
    } else {
        message_bad();
    }
}

//Если пользователь не авторизован то сначала попробуем найти его в базе по email, если не найдем то тогда уже добавим.
else {

    //Запрос на поиск по email.
    $user_item = db::getRow("SELECT users.id, users.name FROM users WHERE users.email = :email", array('email' => $_POST['user_email']));

    //Условие второе. если пользователь нашелся то проводим авторизацию и просто добавляем тему.
    if ($user_item) {
        sign_in_user($user_item['id'], $user_item['name'], $_POST['user_email']);
        if (!empty($_POST['themes_header'])) {
            add_themes($_POST['themes_header'], $user_item['id']);
            message_good();
        } else {
            message_bad();
        }
    }

    //Если пользователь не нашелся то создаем его, авторизовываем и добавляем тему.
    else {
        if (isset($_POST['themes_header']) && isset($_POST['user_name']) && isset($_POST['user_email'])) {
            if (!empty($_POST['themes_header']) && !empty($_POST['user_name']) && !empty($_POST['user_email'])) {
                $user_id = add_user($_POST['user_name'], $_POST['user_email']);
                sign_in_user($user_id, $_POST['user_name'], $_POST['user_email']);
                add_themes($_POST['themes_header'], $user_id);
                message_good();
            } else {
                message_bad();
            }
        }
    }
}

//Подключем подвал.
include "footer.php";
