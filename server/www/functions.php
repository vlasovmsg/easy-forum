<?php
require_once 'db_connect.php';

//Авторизация с помощью кук.
function sign_in_user($user_id, $user_name, $user_email) {
    //Таймаут кук.
    $cookie_time = 1200;

    setcookie("user_name", $user_name, time() + $cookie_time);
    setcookie("user_email", $user_email, time() + $cookie_time);
    setcookie("user_id", $user_id, time() + $cookie_time);
    log_entry("Cookies successfully created for user $user_id");
}

//Добавление пользователя в базу, при успехе возвращаем id вновь созданного пользователя.
function add_user(STRING $user_name, STRING $user_email): INT {
    $user_date = date("Y-m-d");
    $user_data = array('name' => $user_name, 'email' => $user_email, 'date' => $user_date);
    $insert_user = db::addEntry("INSERT INTO users (name, email, date) VALUES (:name, :email, :date)", $user_data);
    log_entry("User $insert_user ($user_name, $user_email) successfully created");
    return $insert_user;
}

//Добавление новой темы в базу, при успехе возвращаем id вновь созданной темы.
function add_themes(STRING $theme_header, INT $theme_author): INT {
    $theme_date = date("Y-m-d H:i:s");
    $theme_data = array('header' => $theme_header, 'author' => $theme_author, 'date' => $theme_date);
    $insert_theme = db::addEntry("INSERT INTO themes (header, author, date) VALUES (:header, :author, :date)", $theme_data);
    log_entry("Themes $insert_theme ($theme_header) successfully created");
    return $insert_theme;
}

//Добавление нового сообщения в теме, при успехе возвращаем id вновь созданной темы.
function add_message(STRING $message_text, INT $message_theme, INT $message_author): INT {
    $message_date = date("Y-m-d H:i:s");
    $message_data = array('text' => $message_text, 'themes' => $message_theme, 'author' => $message_author, 'date' => $message_date);
    $insert_message = db::addEntry("INSERT INTO messages (text, themes, author, date) VALUES (:text, :themes, :author, :date)", $message_data);
    log_entry("Message $insert_message ($message_text) successfully created");
    return $insert_message;
}

function message_good() {
    if (isset($_POST['url'])) {
        $url = $_POST['url'];
        echo "<p>Сообщение успешно добавлено <a href=\"$url\">Вернуться назад</a></p>";
    }
}
function message_bad() {
    if (isset($_POST['url'])) {
        $url = $_POST['url'];
        echo "<p>Заполнены не все поля <a href=\"$url\">Вернуться назад</a></p>";
    }
}

//Функция логирования в текстовый файл.
function log_entry($text) {
    $log_path = "log.txt";
    $date = date('Y-m-d H:i:s');
    $text = "[$date] -> $text";
//    file_put_contents($log_path, $text . PHP_EOL, FILE_APPEND);
}