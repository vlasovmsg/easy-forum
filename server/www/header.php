<?php
// Подключаем основной файл.
require_once 'functions.php';
?>


    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="styles.css">
        <title>Форум</title>
    </head>
<body>


    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <div class="container-fluid">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Главная</a>
                    </li>
                </ul>
            </div>

            <div class="collapse navbar-collapse" id="navbarNav">
                <?php
                if (isset($_COOKIE["user_name"])) {
                    $user_name = $_COOKIE["user_name"];
                    echo "<p class=\"navbar-brand  active\">Приветствуем тебя '$user_name' <a href=\"action_signout.php\">Выйти</a></p>";
                }
                ?>
            </div>
        </div>
    </nav>

    <br>
    <div class="container">