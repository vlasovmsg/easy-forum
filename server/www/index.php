<?php
// Подключем шапку.
include "header.php";
?>


<table class="table table-striped table-hover caption-top">
    <caption>Темы форума</caption>
    <thead class="table-dark">
    <tr>
        <th class="themes_col"     scope="col">Тема</th>
        <th class="themes_author"  scope="col">Автор</th>
        <th class="themes_answer"  scope="col">Последний ответ</th>
        <th class="themes_m_count" scope="col">Всего сообщений</th>
    </tr>
    </thead>
    <tbody>


    <?php

    // ---------------------
    // Получим текущий адрес для формирование ссылки на возрат
    $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    // ---------------------
    // Подготовлаиваем переменные для постраничной навигации.
    // Проверим на какой мы странице и присвоим номер переменной page.
    isset($_GET['page']) ? $page = $_GET['page']: $page = 1;
    // Количество записей на странице для лимита в запрос.
    $count_items_page = 10;
    // Получим общее число записей.
    $count_items_all = db::getValue("SELECT COUNT(*) AS count FROM `themes`");
    // Вычисляем номер первой записи на необходимой странице для установки лимита в запрос.
    $start_item = ($page * $count_items_page) - $count_items_page;
    // Посчитаем сколько нам нужно страниц и округлим до целого в большую сторону.
    $count_page = ceil($count_items_all / $count_items_page);

    // ---------------------
    // Запрос на получение тем форума.
    $themes_items = db::getAll("
        SELECT themes.id       AS themes_id,          -- ID темы для вставик GET запроса в ссылку на тему.
            themes.header      AS `themes_header`,      -- Название темы.
            users1.name        AS `themes_author`,      -- Автор темы.
            COUNT(messages.id) AS `messages_count`,     -- Количество сообщений (благодаря сведению с таблицей сообщений и последующей  группировке по темам).
            MAX(messages.date) AS `messages_last_date`, -- Дата последнего сообщения.
            users2.name        AS `messages_author`     -- Автор последнего сообщения
        FROM `themes`
            LEFT JOIN `messages` ON themes.id = messages.themes        -- Для поседущей группировки по темам и подсчета сообщений в каждой теме.
            LEFT JOIN `users` AS `users1` ON users1.id = themes.author   -- Для получения имени пользователя создавшего тему.
            LEFT JOIN `users` AS `users2` ON users2.id = messages.author -- Для получения имени пользователя последнего сообщения.
        GROUP BY themes.id                   -- Группируем по темам.
        ORDER BY `messages_last_date` DESC   -- Сортируем по дате последнего сообщени в теме в порядке убывания.
        LIMIT $start_item, $count_items_page -- Ограничиваем число записей.
        ");

    // ---------------------
    // Перебираем полученный после запроса массив данных.
    foreach ($themes_items as $item) {
        // Загоняем полученный из массива данные в переменные для более читабельного вида при выводе с помощью echo.
        // Формирем ссылку.
        $themes_link    = "themes.php?id=".$item['themes_id'];
        // Навание темы.
        $themes_header  = $item['themes_header'];
        // Автор темы.
        $themes_author  = $item['themes_author'];
        // Количество сообщений в теме
        $messages_count = $item['messages_count'];
        // Автор последнего сообщения.
        $messages_author = $item['messages_author'];
        // Дата последнего сообщения.
        $messages_last_date = $item['messages_last_date'];

        // Проверяем, если количество сообщений в теме 0 то пишем что "Сообщений нет",
        // инчае выводим информацию о дате и авторе последнего сообщения.
        ($messages_count == 0) ? $messages_data = "Сообщений нет" : $messages_data = date("d.m.y H:i", strtotime($messages_last_date)) . "<br>($messages_author)";

        // Выводим подготовленную информацию.
        echo<<<html
        <tr>
            <th class="themes_col cell" scope="row"><a href="$themes_link">$themes_header</a></th>
            <td class="themes_author cell">$themes_author</td>
            <td class="themes_answer cell">$messages_data</td>
            <td class="themes_m_count cell">$messages_count</td>
        </tr>
        html;
    }
    ?>


    </tbody>
</table>

<nav>
    <ul class="pagination justify-content-center">


        <?php

        // ---------------------
        // Формируем навигацию.
        // Проверяем, если страница не первая, то добавляем слева ссылку на первую.
        if ($page > 1) { echo "<li class=\"page-item\"><a  class=\"page-link\" href=\"index.php?page=1\"> Первая </a></li>"; }

        // Если страниц меньше 3 то нет смысла придумывать переключение страниц.
        if ($count_page > 2) {
            // Хитрости с переключением страниц, дложно быть всегда 3 старинцы для переключения, а текущая была по середине.
            // Допусти у нас страница X тогда логично сделать слева страницу X - 1 а с права X + 1.
            $first_page = $page - 1;
            $last_page = $page + 1;

            // Проверим, если страница X - 1 получилась <= 0 тогда это первая страница и остальные 2 будут справа.
            if ($first_page <= 0) {
                $first_page = 1;
                $last_page = $last_page + 1;
            }
            // А если страница X - 1 получилась >= последней странице тогда это последняя страница и остальные 2 будут слева.
            if ($last_page >= $count_page) {
                $first_page = $count_page - 2;
                $last_page = $count_page;
            }

            // С полученными страницами мы формируем ссылки через цикл.
            for ($i = $first_page; $i <= $last_page; $i++) {
                // Проверим, если i это текущая страница то выводим ее как текст а не как ссылку.
                if ($i == $page) {
                    echo "<li class=\"page-item active\"><p class=\"page-link\" > $i из $count_page </p></li>";
                } else {
                    echo "<li class=\"page-item\"><a class=\"page-link\" href=\"index.php?page=$i\"> $i </a></li>";
                }
            }
        }
        // Если страница не последняя то добавляем ссылку справа на последнюю.
        if ($page < $count_page) { echo "<li class=\"page-item\"><a class=\"page-link\"  href=\"index.php?page=$count_page\"> Последняя </a></li>"; }
        ?>


    </ul>
</nav>

<div class="justify-content-center">
    <h5>Форма добавления темы</h5>
    <form action="action_theme.php" method="post">
        <div class="mb-3">
            <label for="themes_header" class="form-label">Название темы</label>
            <input id="themes_header" class="form-control"  type="text" name="themes_header" placeholder="Заголовок темы">
            <input name="url" type="hidden" value="<?php echo $url ?>"/>
        </div>

        <?php
        // Если у нас сохранилась ссесия то выводить форму для входа пользователя не обязательно.
        if (!isset($_COOKIE["user_name"])) {
            echo<<<html
                <div class="mb-3">
                    <label for="user_name" class="form-label">Имя пользователя</label>
                    <input id="user_name" class="form-control" type="text" name="user_name">
                </div>
                <div class="mb-3">
                    <label for="user_email" class="form-label">E-mail</label>
                    <input id="user_email" class="form-control" type="email" name="user_email">
                    <div class="form-text">Все поля обязательны к заполнению</div>
                </div>
            html;
        }
        ?>


        <input class="btn btn-primary" type="submit" value="Отправить">
    </form>
</div>

<?php
//Подключем подвал.
include "footer.php";
?>