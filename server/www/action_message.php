<?php
// Подключем шапку.
include "header.php";
require_once 'functions.php';

// Проверяем, если пользователь авторизован то добавляем только сообщение, если нет,
// то сообщение и пользователя, заодно авторизовываем его.

//Условие первое, если пользователь авторизован то просто добавляем сообщение.
if (isset($_COOKIE["user_name"])) {
    if (isset($_POST['message_text']) && isset($_POST['message_themes'])) {
        if (!empty($_POST['message_text'])) {
            add_message($_POST['message_text'], $_POST['message_themes'], $_COOKIE["user_id"]);
            message_good();
        } else {
            message_bad();
        }
    }
}

//Если пользователь не авторизован то сначала попробуем найти его в базе по email, если не найдем то тогда уже добавим.
else {

    //Запрос на поиск по email.
    $user_item = db::getRow("SELECT users.id, users.name FROM users WHERE users.email = :email", array('email' => $_POST['user_email']));

    //Условие второе. если пользователь нашелся то проводим авторизацию и просто добавляем сообщение.
    if ($user_item) {
        sign_in_user($user_item['id'], $user_item['name'], $_POST['user_email']);
        if (!empty($_POST['message_text'])) {
            add_message($_POST['message_text'], $_POST['message_themes'], $user_item['id']);
            message_good();
        } else {
            message_bad();
        }
    }

    else {
        if (isset($_POST['message_text']) && isset($_POST['message_themes']) && isset($_POST['user_name']) && isset($_POST['user_email'])) {
            if (!empty($_POST['message_text']) && !empty($_POST['user_name']) && !empty($_POST['user_email'])) {
                $user_id = add_user($_POST['user_name'], $_POST['user_email']);
                sign_in_user($user_id, $_POST['user_name'], $_POST['user_email']);
                add_message($_POST['message_text'], $_POST['message_themes'], $user_id);
                message_good();
            } else {
                message_bad();
            }
        }
    }
}

//Подключем подвал.
include "footer.php";
