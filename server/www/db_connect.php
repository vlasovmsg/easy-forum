<?php

//Класс для работы с БД
class db {
    // Пераметры подключения к базе
    public static $dsn      = "mysql:host=172.10.0.2;port=3306;dbname=easy_forum";
    public static $username = 'root';
    public static $password = '12345678';

    // Объект PDO
    public static $dbh = null;

    // Результат запроса
    public static $sth = null;

    // SQL запрос
    public static $query = '';

    // Подключение к базе
    public static function getDBH() {
        if (!self::$dbh) {
            try {
                self::$dbh = new PDO(
                    self::$dsn,
                    self::$username,
                    self::$password,
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
                );
                self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
                log_entry("Connected successfully.");
            }
            catch (PDOException $error) {
                log_entry("Could not connect to the database: " . $error->getMessage());
                die();
            }
        }
        return self::$dbh;
    }

    // Закрываем соединение
    public static function closeDBH() {
        self::$dbh = null;
        return self::$dbh;
    }

    // Добавление записи в таблицу. В случае упеха вернет id, иначе - 0
    public static function addEntry($query, $param = array()) {
        self::$sth = self::getDBH()->prepare($query);
        return (self::$sth->execute((array) $param)) ? self::getDBH()->lastInsertId() : 0;
    }

    // Получение строки из таблицы
    public static function getRow($query, $param = array()) {
        self::$sth = self::getDBH()->prepare($query);
        self::$sth->execute((array) $param);
        return self::$sth->fetch(PDO::FETCH_ASSOC);
    }

    //Получение всех записей из таблицы
    public static function getAll($query, $param = array()) {
        self::$sth = self::getDBH()->prepare($query);
        self::$sth->execute((array) $param);
        return self::$sth->fetchAll(PDO::FETCH_ASSOC);
    }

    //Получение значения
    public static function getValue($query, $param = array(), $default = null) {
        $result = self::getRow($query, $param);
        if (!empty($result)) {
            $result = array_shift($result);
        }
        return (empty($result)) ? $default : $result;
    }
}
