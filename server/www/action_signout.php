<?php
// Подключем шапку.
include "header.php";
require_once 'functions.php';

if (isset($_COOKIE["user_name"])) {
    setcookie('user_name', '', time()-86400);
    setcookie('user_email', '', time()-86400);
    setcookie('user_id', '', time()-86400);
}

echo "<p>Вы успешно вышли</p>";

//Подключем подвал.
include "footer.php";