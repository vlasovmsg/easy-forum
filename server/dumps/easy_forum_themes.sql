CREATE DATABASE  IF NOT EXISTS `easy_forum` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `easy_forum`;
-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: easy_forum
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `themes` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `header` varchar(256) NOT NULL,
                          `author` int NOT NULL,
                          `date` datetime NOT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `idthemes_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb3 COMMENT='Forum themes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (19,'Строительство и ремонт',8,'2022-05-16 12:42:22'),(20,'Автомобили и грузовики',9,'2022-05-16 12:43:13'),(21,'Настройка роутеров',10,'2022-05-16 12:43:54'),(22,'Воистину радостный звук: песнь светлого будущего',11,'2022-05-16 12:50:55'),(23,'Неподкупность государственных СМИ продолжает удивлять',11,'2022-05-16 12:51:56'),(24,'Добрая половина выводов разочаровала',11,'2022-05-16 12:52:05'),(25,'Частотность поисковых запросов сделала своё дело',11,'2022-05-16 12:52:13'),(26,'Сейчас всё чаще звучит далёкий барабанный бой',11,'2022-05-16 12:52:23'),(27,'Базовый вектор развития даёт нам право принимать самостоятельные решения',11,'2022-05-16 12:52:31'),(28,'Цена вопроса не важна, когда помыслы поколения чисты',11,'2022-01-16 12:52:39'),(29,'Как бы то ни было, убеждённость некоторых оппонентов позволила расправить крылья',11,'2022-03-16 12:52:47'),(30,'Консультация с широким активом оказалась ошибочной',11,'2022-02-16 12:52:56'),(31,'Органический трафик связывает нас с нашим прошлым',11,'2022-04-16 12:53:06');
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-16 18:09:43
